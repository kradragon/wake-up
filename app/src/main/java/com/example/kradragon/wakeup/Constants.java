package com.example.kradragon.wakeup;

public interface Constants {
    String TURN_OFF_SCREEN_ACTION = "com.example.kradragon.wakeup.TURN_OFF_SCREEN_ACTION";
    String ACTION_UNREGISTER_LISTENER = "unregister_listener";
    String ACTION_REGISTER_LISTENER = "register_listener";
}
